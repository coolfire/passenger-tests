#!/bin/bash
docker run --rm -d -p 8080:80 --name passenger-stable-deb passenger-stable-deb:latest
docker run --rm -d -p 8081:80 --name passenger-stable-nodesource passenger-stable-nodesource:latest

echo "Default debian nodejs with patch: http://localhost:8080"
echo "Upstream nodejs with patch: http://localhost:8081"
