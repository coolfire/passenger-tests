# Docker containers to test GLOBAL not defined fix

A docker setup to easily validate the fix actually fixes the issue and doesn't break on older versions.

The "patch" being tested out is only replacing the `GLOBAL` alias with `global` in `/usr/share/passenger/helper-scripts/node-loader.js`. The "patch" is applied by running `sed -i 's/GLOBAL/global/' /usr/share/passenger/helper-scripts/node-loader.js` in the Dockerfile for each container.

Both containers use a nodejs connect demo application from https://github.com/phusion/passenger-nodejs-connect-demo to check node applications are running.

## Bugreport
https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1025220

## Instructions

### Building
Run the `build.sh` bash script. This will build two containers:
 - **passenger-stable-deb**: Debian stable base image with nodejs from debian repos and "sed patch" applied
 - **passenger-stable-nodesource**: Debian stable base image with LTS nodejs from nodesource and "sed patch" applied

### Running
Run the `run.sh` bash script. It will start both containers, the debian nodejs on localhost:8080 and the nodesource nodejs on localhost:8081.

### Stopping & cleanup
Run the `stop.sh` bash script. This just stops both containers. You like want to `docker image prune` or `docker image rm passenger-stable-deb` and `docker image rm passenger-stable-nodesource` to clean up image caches when done.
