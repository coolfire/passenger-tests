#!/bin/bash
docker stop passenger-stable-deb
docker stop passenger-stable-nodesource

# Maybe run the below image cleanup commands
# docker image rm passenger-stable-deb
# docker image rm passenger-stable-nodesource
