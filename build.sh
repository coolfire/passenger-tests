#!/bin/bash
docker build -f dockerfiles/Dockerfile.stable.deb -t passenger-stable-deb:latest .
docker build -f dockerfiles/Dockerfile.stable.nodesource -t passenger-stable-nodesource:latest .
